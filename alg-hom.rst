===============================
Algebra homomorphism CSP
===============================

**Authors**. William DeMeo, Ralph Freese 

**Address**. ``williamdemeo@gmail.com``, ``ralph@math.hawaii.edu``

**Date**. 30 Apr 2020

------------------------------------------------

Introduction
-------------

This note concerns the complexity of some constraint satisfaction problems (CSPs) involving finite algebraic structures of a fixed finite "signature", 𝑆, also known as 𝑆-structures. (These and other technical terms will be defined in due course.)

A common approach to constraint satisfaction problems is to model them as decision problems in which one must decide whether there exist homomorphisms from one algebraic or relational (or general algebraic+relational) structure to another.  A general form of the problem would, for example, take as input a pair (𝐀, 𝐁) of structures (of the same fixed signature)---the input is sometimes called an "instance" of the problem---and would output "yes" if there exists a homomorphism from 𝐀 to 𝐁, and output "no" otherwise.

It is equivalent to view this as a membership problem. Indeed, if :math:`\mathrm{HOM}_𝑆` denotes the collection of pairs (𝐂, 𝐃)  of structures (of fixed signature 𝑆) for which there exists a homomorphism from 𝐂 to 𝐃, then given an instance (𝐀, 𝐁) we must decide whether (𝐀, 𝐁) ∈ :math:`\mathrm{HOM}_𝑆`.

This problem is intractable in general, but much work has been done to determine the computational complexity of restricted versions of this problem.  Below we review and extend a limited selection of this work.

---------------------------------------------------


Basic definitions
------------------

By a **signature** we mean a quadruple 𝑆 = (𝐶, 𝐹, 𝑅, ρ) consisting of 

* three disjoint sets

  * 𝐶 = constant symbols
  * 𝐹 = operation symbols
  * 𝑅 = relation symbols

* one function ρ : 𝐶 + 𝐹 + 𝑅 → ℕ called the **arity function** of 𝑆, which satisfies the following condition: ρ(𝑠) = 0 iff 𝑠 = ι₀ 𝑐 for some 𝑐 ∈ 𝐶. [1]_ 

Let ℱ := 𝐶 + 𝐹 + 𝑅.  By a **structure in the signature** 𝑆, we mean a pair :math:`𝐀 = (𝐴 , ℱ^𝐀)` where:

* 𝐴 is a set (called the "universe" of the structure) and 
* :math:`ℱ^𝐀` is a set that contains, for each symbol 𝑠 ∈ ℱ, an interpretation :math:`s^𝐀` of that symbol satisfying the following conditions:

  * if 𝑠 ∈ 𝐶, then :math:`s^𝐀 ∈ 𝐴`;
  * if 𝑠 ∈ 𝐹, then :math:`s^𝐀 : 𝐴^{ρ (s)} → A`;
  * if 𝑠 ∈ 𝑅, then :math:`s^𝐀 ⊆ 𝐴^{ρ (s)}`.

[2]_ 


The general finite structure homomorphism problem
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Let 𝒮 denote the category of 𝑆 structures and let Hom(-,-) : 𝒮 :math:`^{\mathrm{op}}` × 𝒮 → **Set** be the usual hom functor that takes a pair (𝐀, 𝐁) of 𝑆-structures to the set of homomorphism from 𝐀 to 𝐁. [3]_ 

Let :math:`\mathrm{HOM}_𝑆` denote the collection of pairs (𝐀, 𝐁) of 𝑆-structures such that there exists a homomorphism from 𝐀 to 𝐁.  That is, 

.. math:: \mathrm{HOM}_𝑆 := \{ (𝐀, 𝐁) : \mathrm{Hom}(𝐀, 𝐁) ≠ ∅ \}.

The **general finite structure homomorphism problem** (which we denote by :math:`\mathrm{HOM}_𝑆`) is the decision problem with

* **input**: a pair (𝐀, 𝐁) of finite 𝑆-structures;
  
* **output**: (a decision)

  * **yes**, if there exists a homomorphism from 𝐀 to 𝐁;
 
  * **no**, otherwise. 

  i.e., **yes** if (𝐀, 𝐁) ∈ :math:`\mathrm{HOM}_𝑆`; **no**, if Hom(𝐀, 𝐁) = ∅.

The problem :math:`\mathrm{HOM}_𝑆` is NP-complete in general, but if we restrict 𝑆 or we restrict the input to 𝑆-structures from a particular variety, for example, then the problem may be tractable.
(todo: insert references to Nesetril-Hell, Schaefer, etc.)

Instead of, or in addition to, restricting the problem by focusing on special classes of structures, we may fix in advance either the domain or codomain of the potential homomorphism.

The fixed codomain finite structure homomorphism problem
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For a fixed finite structure 𝐁 (the "template"), Hom(-,𝐁) : 𝒮 :math:`^\mathrm{op}` → Set denotes the usual contravariant hom functor. Let :math:`\mathrm{HOM}_𝑆(-,𝐁)` be the collection of structures from which there is a homomorphism into 𝐁.  

The **fixed codomain finite structure homomorphism problem** (which we will denote by :math:`\mathrm{HOM}_𝑆(-,𝐁)` is the decision problem with

* **input**: a finite 𝑆-structure 𝐀;
  
* **output**:

  * **yes**, if there exists a homomorphism from 𝐀 to 𝐁;
 
  * **no**, otherwise.

  i.e., **yes**, if 𝐀 ∈ :math:`\mathrm{HOM}_𝑆(-, 𝐁)`; **no**, if Hom(𝐀,𝐁) = ∅.

In general, the problem :math:`\mathrm{HOM}_𝑆(-,𝐁)` is NP-complete, but may be tractable if we restrict 𝐁 and/or the class of structures that may appear in the input.  We investigate this problem and restricted versions of it in Section XX.

The fixed domain finite structure homomorphism problem
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For a fixed finite structure 𝐀 (the "template"), Hom(𝐀,-) : 𝒮 → **Set** denotes the usual covariant hom functor. Let :math:`\mathrm{HOM}_𝑆(𝐀,-)` be the collection of structures into which there is a homomorphism from 𝐀.

The **fixed domain finite structure homomorphism problem** (which we will denote by :math:`\mathrm{HOM}_𝑆(𝐀,-)` is the decision problem with

* **input**: a finite 𝑆-structure 𝐁;
  
* **output**:                 

  * **yes**, if there exists a homomorphism from 𝐀 to 𝐁;
 
  * **no**, otherwise.

  i.e., **yes** if 𝐁 ∈ :math:`\mathrm{HOM}_𝑆(𝐀,-)`; **no** if Hom(𝐀, 𝐁) = ∅.

The problem :math:`\mathrm{HOM}_𝑆(𝐀,-)` is NP-complete in general, but may be tractable if we restrict 𝑆 and/or the class of structures that may appear in the input. For example, if we restrict the signature 𝑆 = (𝐶, 𝐹, 𝑅, ρ) so that 𝑅 is empty, and thus we consider only (finite, finitary) algebraic structures, then we can prove :math:`\mathrm{HOM}_𝑆(𝐀,-)` is tractable.

**Prop**. Let 𝑆 = (𝐶, 𝐹, ∅, ρ), where 𝐶 and 𝐹 are finite sets. Then the problem :math:`\mathrm{HOM}_𝑆(𝐀,-)` has time complexity that is polynomial in the size of the input.

**Proof**. Given an instance 𝐁 (a finite 𝑆-algebra), assume the universes of 𝐀 and 𝐁 have respective cardinalities 𝑘 and 𝑛. Then the number of maps from the set 𝐴 (the universe of 𝐀) to the set 𝐵 is :math:`n^k`. Let :math:`𝑓 : 𝐴 → 𝐵` be one such map. Let 𝑋 be a subset of 𝐴 and suppose that for each 𝑥 ∈ 𝑋 we know the value of 𝑓(𝑥) ∈ 𝐵. (We put aside for now the cost of accessing or computing such 𝑓(x) values.)

There exists a polynomial- (in fact, linear-) time algorithm to decide whether the restriction of 𝑓 to 𝑋 extends to a homomorphism from 𝐀 to 𝐁.


----------------------------------------------

Acknowledgements
-----------------

The first author would like to thank Libor Barto and the CoCoSym Grant for suppoting this work.

-----------------

.. rubric:: Footnotes

.. [1]
   The symbol + denotes the disjoint union and ℕ denotes the set of natural numbers.

.. [2]
   You can ignore the ιᵢ symbol if you don't care about technical precision; we use it to denote the function that embeds its argument into the 𝑖-th summand of a direct sum. Note that we have used a 0-offset subscript so "the first summand" is the one with 0 subscript---"the 0-th summand." The condition ρ (𝑠) = 0 iff s = ι₀ 𝑐 for some 𝑐 ∈ 𝐶 simply stipulates that only constants are nullary; operations and relations have positive arity.  This is not essential and is merely a choice made for the sake of convenience. 

.. [3]
   We won't need any category theory here, but it is sometimes helpful to use category theory terminology when defining or denoting certain things.

.. For a fixed (finite) algebra :math:`𝐁 = (B , 𝐹^𝐁)` (called the CSP "template"), let **HOM(-,𝐁)** denote the decision problem with
.. * **input**: an algebra :math:`𝐀 = (A , 𝐹^𝐀)` of signature 𝑆;
.. * **output**: a decision,
..  * **yes**, if there exists a homomorphism from 𝐀 to 𝐁;
..  * **no**, otherwise.
