Complexity of Algebra Homomorphisms
=====================================

We consider the following decision problem:

Given algebras A and B

Decide whether there exists a homomorphism h : A -> B.
